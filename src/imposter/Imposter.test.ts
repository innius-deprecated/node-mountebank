import {DefaultImposter, Imposter} from "./Imposter"
import {HttpMethod} from "../HttpMethod";
import {Stub} from "../stub/Stub"
import {assert} from "chai"

describe("DefaultImposter", () => {
    let imp = new DefaultImposter("/path",HttpMethod.POST, { name: "test123" }, 200);

    it("should have a default stub", () => {
        assert.isNotNull(imp.stubs[0]);
    })
    describe("#Create", () => {
        before((done) => {
            imp.create((err: Error, im: Imposter) => {
                imp = im;
                done(err);
            });
        })
        it("should create a new imposter", () => {
            assert.isNotNull(imp.port);
        })
    })
})
