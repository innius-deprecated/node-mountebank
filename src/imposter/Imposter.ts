import {Response, NotFoundResponse} from "../response/Response";
import {HttpMethod} from "../HttpMethod";
import {Stub, DefaultStub} from "../stub/Stub";
import * as http from "http";

export type callback = (err: Error, imposter: Imposter) => void;

export class Imposter {
    public protocol: string = "http";
    public port: number;
    public stubs: Stub[];
    constructor() {
        this.stubs = [];
    }

    withStub(s: Stub): Imposter {
        this.stubs.push(s);
        return this;
    }

    withPort(p: number): Imposter {
        this.port = p;
        return this;
    }

    create(cb: callback): void {
        var options = {
            port: 2525,
            path: "/imposters",
            method: "POST",
        };

        http.request(options, (res) => {
            let data = "";
            res.on("data", (chunk: string) => {
                data += chunk;
            });
            res.on("end", () => {
                cb(null, <Imposter>JSON.parse(data));
            });
            res.on("error", (err: Error) => {
                cb(err, null);
            });
        }).end(JSON.stringify(this));

    }
}

export class DefaultImposter extends Imposter {
    constructor(path: string, method: HttpMethod, body: any, statusCode: number) {
        super();
        
        return this
            .withStub(new DefaultStub(path, method, body, statusCode))
            .withStub(new Stub().withResponse(new NotFoundResponse()));
    }
}
