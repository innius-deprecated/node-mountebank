#node-mountebank 

A simple node package that lets you manage your [Mountebank test server](http://mbtest.org).

## Installation

```typescript
go get "bitbucket.org/to-increase/go-mountebank"

import(mb "bitbucket.org/to-increase/go-mountebank")
```

## Usage

### Pre-Requisite

Install Mountebank:

```
npm install -g mountebank --production
```

Start Mountebank:

```
mb 
```

I recommend reading the [Mountebank documentation](http://www.mbtest.org/docs/api/overview) for a deeper understanding of their API.

### Create Imposter
```typescript
imp := mb.NewImposter().WithPort(1974)WithStub(stb)
err := imp.Create()
```
 
### Create a Stub 
```go
stb := mb.NewStub()
```

### Create a new Predicate
```go
    mb.NewPredicate().WithEquals().
        WithMethod("GET").
        WithPath("/test123").
		WithHeader("Content-Type", "application/json").
		WithHeader("Authorization", "bearer xxx")
```

### Create a new Response 
```go
    emp := &Employee{}
    mb.NewResponse().
		WithStatusCode(200).
		WithBody(emp).
		WithHeader("Conent-Type", "application/text"))
```

### Create a new Response with a wait behavior 
```go
    emp := &Employee{}
    mb.NewResponse().
		WithStatusCode(200).
		WithBody(emp).
        WithWait(1000).
		WithHeader("Conent-Type", "application/text"))
```


### Using mountebank in go tests 

check if mountebank is running in TestMain
```go
    func TestMain(m *testing.M) {
        if !mb.IsRunning() {
            panic(fmt.Errorf("Mountebank is not running; see http://mbtest.org for installation instructions"))
        }
        os.Exit(m.Run())
    }
```

add a configuration 
```go
    imp, err := mb.NewImposter().WithStub(mb.NewStub().WithResponse(mb.NewResponse().WithBody(b))).Create()
    assert.Nil(t, err)
```    
    
this returns an imposter with a randomly assinged port number;
```go
client := authentication.New(config.NewConfig().WithEndpoint(fmt.Sprintf("http://localhost:%v", imp.Port)))
```  

create a default imposter 
```go
    emp := &Employee{}
    imp, err := mb.DefaultPostImposter("/item", emp, http.StatusCreated)
```
